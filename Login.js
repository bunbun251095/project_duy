import axios from "axios";
import { Component } from "react";
import ErrorRegister from "../ErrorRegister";
import Register from "./Register";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            email:"",
            password:"",
            phone:"",
            address:"",
            file:"",
            level:0,
            avatar:"",
            fromErrors:{},
            data:""
         }
         this.handleInput=this.handleInput.bind(this)
         this.handleImage=this.handleImage.bind(this)
         this.handleSubmit=this.handleSubmit.bind(this)
    }
    handleInput(e){
        const nameInput = e.target.name
        const value = e.target.value
        this.setState({
            [nameInput]:value
        })
    }
    handleImage(e){
        let file = e.target.files
        // console.log(file)
        let reader = new FileReader();
        reader.onload= (e) => {
            this.setState({
                avatar:e.target.result, // cai nay de gui qua api
                file:file[0] // luu thong tin file vafo file de check
            })
        }
        reader.readAsDataURL(file[0])
    }

    handleSubmit(e){
        e.preventDefault()
        let flag = true
        let errorSubmit = this.state.fromErrors
        let {name,email,password,address,phone,file} = this.state
        
        errorSubmit.name = errorSubmit.email = errorSubmit.password = errorSubmit.address = errorSubmit.phone
        if(!name){
            flag = false
            errorSubmit.name = "Vui long nhap name"
        }
        if(!email){
            flag = false
            errorSubmit.email = "Vui long nhap email"
        }
        if(!password){
            flag = false
            errorSubmit.password = "Vui long nhap password"
        }
        if(!address){
            flag = false
            errorSubmit.address = " vui long nhap address"
        }
        if(!phone){
            flag = false
            errorSubmit.phone = " vui long nhap phone"
        }
        if(!phone){
            flag = false
            errorSubmit.phone = " vui long nhap phone"
        }
        if(file == ""){
            errorSubmit.avatar="vui long chon hinh anh"
            flag = false
        }else{
            let regex = ["png", "jpg", "jpeg", "PNG", "JPG"]
            if(file.size > 1024*1024){
                flag=false
                errorSubmit.avatar = "size qua lon"
            }else if(!regex.includes(file.name.split(".").pop())){
                flag=false
                errorSubmit.avatar = "file khong dung dinh dang anh"
            }
        }


        if(!flag){
            this.setState({
                fromErrors:errorSubmit
            })
        }else{
            const data ={
                name:this.state.name,
                email:this.state.email,
                password:this.state.password,
                address:this.state.address,
                phone:this.state.phone,
                avatar:this.state.avatar,
                level:0
            }
         axios.post("http://laravel-api.com/api/register", data)
         .then(response => {
            // console.log(response)
            // console.log(response.data)
            if(response.data.errors){
                this.setState({
                    fromErrors:response.data.errors
                })
            }else{
                this.setState({
                name:"",
                email:"",
                password:"",
                address:"",
                phone:"",
                avatar:"",
                file:"",
                })
            }
        })
        }
    }
    render() { 
        return (
        <div className="row">
            <Register/>
                <div className="col-sm-4">
                    <div className="signup-form">{/*sign up form*/}
                    <h2>New User Signup!</h2>
                    <ErrorRegister fromError = {this.state.fromErrors} />
                    <form onSubmit={this.handleSubmit} enctype="multipart/form-data">
                            <input type="text" name="name" value={this.state.name} placeholder="Name" onChange={this.handleInput} />
                            <input type="email" name="email" value={this.state.email} placeholder="Email Address" onChange={this.handleInput} />
                            <input type="password" name="password" value={this.state.password} placeholder="Password" onChange={this.handleInput} />
                            <input type="text" name="phone" value={this.state.phone} placeholder="Phone" onChange={this.handleInput} />
                            <input type="text" name="address" value={this.state.address} placeholder="Address" onChange={this.handleInput}/>
                            <input  type="file"  name="avatar" onChange={this.handleImage}  multiple/>
                            <input  type="text" name="lever"  value={this.state.level}  />
                            <button type="submit" className="btn btn-default">Signup</button>
                        </form>
                </div>{/*/sign up form*/}
            </div>
        </div>      
         );
    }
}
 
export default Login;