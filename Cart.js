import axios from "axios";
import { Component } from "react";
import CartList from "./CartList";

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = { 

         }
        
    }
    render() { 
        return ( 
            <section id="cart_items">
            <div className="container">
              <div className="breadcrumbs">
                <ol className="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li className="active">Shopping Cart</li>
                </ol>
              </div>
              <div className="table-responsive cart_info">
                <table className="table table-condensed">
                  <thead>
                    <tr className="cart_menu">
                      <td className="image">Item</td>
                      <td className="description" />
                      <td className="price">Price</td>
                      <td className="quantity">Quantity</td>
                      <td className="total">Total</td>
                      <td />
                    </tr>
                  </thead>
                  <tbody>
                    <CartList/>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
         );
    }
}
 
export default Cart;




