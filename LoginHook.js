import axios from "axios";
import { Component } from "react";
import ErrorRegister from "../ErrorRegister";
import { withRouter } from "react-router-dom";
import { AppContext } from "../../context/AppContext";


class Register extends Component {
  static contextType = AppContext
    constructor(props) {
        super(props);a
        this.state = { 
          email:"",
          password:"",
          lever:0,
          data:"",
          fromErrors:{},
        
          
         }
         this.handleInputlogin=this.handleInputlogin.bind(this)
         this.handleSubmit=this.handleSubmit.bind(this)
    }
    handleInputlogin(e){
      let nameInput=e.target.name
      let valueInput=e.target.value
      this.setState({
        [nameInput]:valueInput
      })
    }
  handleSubmit(e){
    e.preventDefault()
    let flag = true
    let errorSubmit = this.state.fromErrors
    let {email,password} = this.state

    if(!email){
      flag = false
      errorSubmit.email = "Vui long nhap email"
    }
    if(!password){
      flag = false
      errorSubmit.password = "Vui long nhap password"
    }
    if(!flag){
      this.setState({
        fromErrors:errorSubmit
    })
    }else{
      const data={
        email:this.state.email,
        password:this.state.password,
        lever:this.state.lever
      }
      axios.post("http://laravel-api.com/api/login", data)
      .then(response => {
        if(response.data.errors){
            this.setState({
                fromErrors:response.data.errors
            })
        }else{
          this.setState({
            email:"",
            password:"",
            lever:0
          })
          this.context.checkLoin(true)
          localStorage.setItem("User",JSON.stringify(response.data))
          this.props.history.push('/')

         }
       })
      }
     }
    render() {
      console.log(this.context)
        return ( 
        <div>
        <div className="col-sm-4 col-sm-offset-1">
          <div className="login-form">{/*login form*/}
            <h2>Login to your account</h2>
            <ErrorRegister fromError = {this.state.fromErrors} />
            <form onSubmit={this.handleSubmit}>
              <input type="email" name="email" value={this.state.email} placeholder="Email Address" onChange={this.handleInputlogin} />
              <input type="password" name="password" value={this.state.password} placeholder="password"onChange={this.handleInputlogin} />
              <input  type="text" name="lever"  value={this.state.lever}  />
              <span>
                <input type="checkbox" className="checkbox" /> 
                Keep me signed in
              </span>
              <button type="submit" className="btn btn-default">Login</button>
            </form>
          </div>{/*/login form*/}
        </div>
        <div className="col-sm-1">
          <h2 className="or">OR</h2>
        </div>
      </div>
         );
    }
}
 
export default withRouter (Register)
//duyln.rtbed@gmail.com