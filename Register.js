import { useState } from "react";
import ErrorRegister from "../ErrorRegister";

function LoginHook() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [lever, setLever] = useState(0)
    const [fromErrors, setFormErros] = useState({})

    const handleSubmit = (e) =>{
      e.preventDefault()
      let errorSubmit = {}
      let flag = true

      console.log(email)
      if(email==""){
        errorSubmit.email = "Vui long nhap email"
        flag = false
      }
      setFormErros(errorSubmit)
      // if(flag == false){
      //   setFormErros(errorSubmit)
      // }


    }

    return ( 
        
        <div>
        <div className="col-sm-4 col-sm-offset-1">
          <div className="login-form">{/*login form*/}
            <h2>Login to your account</h2>
            <ErrorRegister fromError={fromErrors}/>
            <form onSubmit={handleSubmit} >
              <input type="email" name="email" value={email} placeholder="Email Address" onChange={(e)=> setEmail(e.target.value)} />
              <input type="password" name="password" value={password} placeholder="password"onChange={(e)=> setPassword(e.target.value)} />
              <input  type="text" name="lever"  value={lever}  />
              <span>
                <input type="checkbox" className="checkbox" /> 
                Keep me signed in
              </span>
              <button type="submit" className="btn btn-default">Login</button>
            </form>
          </div>{/*/login form*/}
        </div>
        <div className="col-sm-1">
          <h2 className="or">OR</h2>
        </div>
      </div>
     );
}

export default LoginHook;





