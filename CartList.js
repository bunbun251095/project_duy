import axios from "axios";
import { Component } from "react";

class CartList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          data:{},
          dataLocal:{}
         }
         this.hanlndUp = this.hanlndUp.bind(this)
         this.hanlndDown = this.hanlndDown.bind(this)
         this.hanlndDelete= this.hanlndDelete.bind(this)
         this.handleInput=this.handleInput.bind(this)
    }
    componentDidMount(){
        let getLocal = JSON.parse(localStorage.getItem("cardProduct"))
        axios.post("http://localhost/laravel/public/api/product/cart",getLocal)
            .then(res =>{
               this.setState({
                data:res.data.data,
                dataLocal:getLocal
               })
            })
    }

    renderData(){
      let {data} = this.state
      if(Object.keys(data).length>0){
        return Object.keys(data).map((key,item)=>{
          let image = JSON.parse(data[key]["image"])
          return(
            <tr>
            <td className="cart_product">
                <a  href><img className="sizeImag" src={"http://localhost/laravel/public/upload/user/product/"+data[key]["id_user"]+"/"+ image[0]}/></a>
              </td>
              <td className="cart_description">
                <h4><a href>{data[key]["name"]}</a></h4>
                <p>Web ID: {data[key]["web_id"]}</p>
              </td>
              <td className="cart_price">
                <p>${data[key]["price"]}</p>
              </td>
              <td className="cart_quantity">
                <div className="cart_quantity_button">
                  <a className="cart_quantity_up" id={data[key]["id"]} onClick={this.hanlndUp}> + </a>
                  <input type="number" className="cart_quantity_input"  id={data[key]["id"]} name="quantity" value={data[key]["qty"]} onChange={this.handleInput} autoComplete="off" size={2} />
                  <a className="cart_quantity_down" id={data[key]["id"]} onClick={this.hanlndDown}> - </a>
                </div>
              </td>
              <td className="cart_total">
                <p className="cart_total_price">${(data[key]["qty"]) * (data[key]["price"]) }</p>
              </td>
              <td className="cart_delete">
                <a className="cart_quantity_delete" ><i id={data[key]["id"]} onClick={this.hanlndDelete} className="fa fa-times" /></a>
            </td>
          </tr>
          )
        })
      }
    }
    //input
    handleInput(e){
      let idProduct = e.target.id
      let getValue =  parseInt(e.target.value)
      let {dataLocal,data} = this.state
      
      Object.keys(dataLocal).map((key,item)=>{
        if(key == idProduct){
          dataLocal[idProduct] =  parseInt(getValue)
         console.log(dataLocal[key])
        }
      })
      localStorage.setItem("cardProduct",JSON.stringify(dataLocal))
      Object.keys(data).map((key,item)=>{
        if(data[key]["id"] == idProduct){
          data[key]["qty"] = getValue
        }
      })
     this.setState({
       data:data
     })
    }



    //Up
    hanlndUp(e){
      let idProduct = e.target.id
      let {dataLocal,data} = this.state

      Object.keys(dataLocal).map((key,item)=>{
        if(key == idProduct){
          dataLocal[key] +=1
        }
      })
      localStorage.setItem("cardProduct",JSON.stringify(dataLocal))
      Object.keys(data).map((key,item)=>{
        if(data[key]["id"] == idProduct){
          data[key]["qty"] +=1
        }
      })
     this.setState({
       data:data
     })
    }
    

    //Down
    hanlndDown(e){
      let idProduct = e.target.id
      let {dataLocal,data} = this.state

      Object.keys(dataLocal).map((key,item)=>{
        if(key == idProduct){
          dataLocal[key]-=1
          if(dataLocal[key] < 1){
            delete dataLocal[idProduct]
          }
        }
      })
      localStorage.setItem("cardProduct",JSON.stringify(dataLocal))
      Object.keys(data).map((key,item)=>{
        if(data[key]["id"] == idProduct){
          data[key]["qty"] -=1
          if(data[key]["qty"] < 1){
           delete data[key]
          }
        }
      })
     this.setState({
       data:data
     })
    }

    
    //Delete
    hanlndDelete(e){
      let idProduct = e.target.id
      let {dataLocal,data} = this.state
      delete dataLocal[idProduct]
      
      localStorage.setItem("cardProduct",JSON.stringify(dataLocal))
      Object.keys(data).map((key,item)=>{
        if(data[key]["id"] == idProduct){
          delete data[key]
        }
      })
     this.setState({
       data:data
     })
    }
   


    render() { 
        return ( 
            <div>
                 {this.renderData()}
            </div>
         );
    }
}
 
export default CartList;